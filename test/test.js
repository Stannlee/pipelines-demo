const chai = require('chai')
const chaiHttp = require('chai-http')
chai.use(chaiHttp)
const app = require('../app')
const http = chai.request(app).keepOpen()
const expect = chai.expect

describe('API test', () => {
    it('The API root should respond with 200', (done) => {
        http.get('/api')
            .then((res) => {
                expect(res.status).equals(200)
                done()
            }).catch((error) => {
                done(error)
            })
    })

    it('Should get a list of users', (done) => {
        http.get('/api/users')
            .then((res) => {
                expect(res.status).equals(200)
                done()
            })
            .catch((error) => {
                done(error)
            })
    })
})