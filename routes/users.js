const express = require('express');
const router = express.Router();

/* GET users listing. */
router.get('/', function (req, res, next) {
  const users = [
    {
      name: "John Doe",
      occupation: "Hacker"
    },
    {
      name: "Jane Doe",
      occupation: "Sofware Dev"
    }
  ]
  res.json(users)
});

module.exports = router;
